' Mandelbrot Set Project
' Quite BASIC Math Project
' ------------------------ 

REM Subroutine -- Set up Palette
def SetupPalette(P)
  LET P(0) = 0x000000 ' "black"
  LET P(1) = 0xFF0000 ' "blue"
  LET P(2) = 0xFF6600 ' "cyan"
  LET P(3) = 0X00FF00 ' "green"
  LET P(4) = 0xFFFF00 ' "yellow"
  LET P(5) = 0xDDDDDD ' "light-grey"
  LET P(6) = 0x0000FF ' "red"
  LET P(7) = 0xFFFFFF ' "white"
enddef 

CLS()
gfx.color(0x000000)
gfx.clear()

PRINT "This program plots a graphical representation of the famous "
PRINT "Mandelbrot set.  It takes a while to finish so have patience."

REM Initialize the color palette
dim P(9)
SetupPalette(P)

REM L is the maximum iterations to try
LET L = 100
FOR I = 0 TO 199
  FOR J = 0 TO 199
    REM Map from pixel coordinates (I,J) to math (U,V)
    LET U = I / 100 - 1.5
    LET V = J / 100 - 1
    LET X = U
    LET Y = V
    LET N = 0
    REM Inner iteration loop starts here
    WHILE N<L
      LET R = X * X
      LET Q = Y * Y
      IF R + Q > 4 THEN EXIT
      LET Y = 2 * X * Y + V
      LET X = R - Q + U
      LET N = N + 1
    WEND

    REM Compute the color to plot
    IF N < 10 THEN
      gfx.color(P(0))
    else
      gfx.color(P(ROUND(7 * (N-10) / (L-10))))
    endif
    gfx.Draw (I, J, I, J) 
  NEXT J
NEXT I

END
