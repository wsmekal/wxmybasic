f = fopen("file.txt", "w+")

fwrite(f, "123")               ' Write a string
fwrite(f, 10, 13)              ' Write \n\r
fwrite_line(f, "hello world!") ' Write a line

print fpeek(f);

fpoke(f, 0)

s = fread(f, 3)                ' Read a string length 3
n = fread(f)                   ' Read a byte
r = fread(f)                   ' Read another byte
l = fread_line(f)              ' Read a line

print fpeek(f);

print s, n, r, l;              ' Note bytes are printed as int

fclose(f)