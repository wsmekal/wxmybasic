
gfx.color(0xFFFFFF)
gfx.clear()

for r = 0 to 255 step 1
  gfx.color(gfx.RGB(r, 0, 0))
  gfx.draw(20+r, 20, 20, 100)
next r

width=320
height=200

gfx.color(0x000000)
gfx.draw(      0,        0, width-1,        0)
gfx.draw(width-1,        0, width-1, height-1)
gfx.draw(width-1, height-1,       0, height-1)
gfx.draw(      0, height-1,       0,        0)
