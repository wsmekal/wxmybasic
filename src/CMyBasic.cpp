/*! @file CMyBasic.cpp
 *  @brief Short explanation
 *
 *  Long explanation
 *
 *  Changes:
 *  2020.06.05 (ws): Initial file.
 */

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

// std headers
#include <stdarg.h>

// wxMyBASIC headers
#include "CMyBasic.h"
#include "app.h"
#include "frame.h"
#include "CPaintFrame.h"
#include "CConsoleFrame.h"
#include "CDrawPanel.h"


void SendConsoleText(wxString text)
{
  // post message with text to pe printed
  CConsoleMessage newMsg = {outputText, text};
  wxGetApp().console->mMsgQueue.Post(newMsg);

  // send event to queue
  wxCriticalSectionLocker enter(wxGetApp().console->mEmptyQueueCS);

  if (wxGetApp().console->mEmptyQueue)
  {
    wxQueueEvent(wxGetApp().console, new wxThreadEvent(wxEVT_THREAD, wxEVT_CONSOLE_MESSAGE));
    wxGetApp().console->mEmptyQueue = false;
  }
}


void SendInfoText(wxString text)
{
  wxThreadEvent* event = new wxThreadEvent(wxEVT_THREAD, wxEVT_OUTPUT_INFO);
  event->SetString(text.c_str());

  wxQueueEvent(wxGetApp().frame, event);
}


int __Print(const char* fmt, ...) {
  int result = 0;
  va_list argptr;

  wxString printStr;
  va_start(argptr, fmt);
  result = printStr.PrintfV(fmt, argptr);
  if (result < 0) {
    SendInfoText("Encoding error.\n");
  }
  va_end(argptr);

  if (result >= 0)
    SendConsoleText(printStr);

  return result;
}


int __Input(const char* pmt, char* buf, int s)
{
  int result = 0;
  CMyBasicMessage msg;

  // clear buffer
  memset(buf, '\0', s);

  // first clear message queue
  wxGetApp().myBasic->GetMsgQueue()->Clear();

  // ask GUI for input string
  wxThreadEvent* event = new wxThreadEvent(wxEVT_THREAD, wxEVT_ASK_FOR_INPUT);
  event->SetString(pmt);
  wxQueueEvent(wxGetApp().frame, event);

  // wait until message returns in queue
  while (wxGetApp().myBasic->GetMsgQueue()->ReceiveTimeout(100, msg) == wxMSGQUEUE_TIMEOUT)
  {
    // if process is stopped, return 0 (will break in next line)
    if (wxGetApp().frame->GetThread()->TestDestroy())
      return 0;
  }

  // replace all cr and ln and determine length
  msg.text.Replace("\n", "");
  msg.text.Replace("\r", "");
  result = msg.text.length();

  // limit return string
  if (result > s - 1)
    result = s - 1;

  // copy returned string to buffer
  strncpy(buf, msg.text.c_str(), result);

  return result;
}


int __CLS(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_attempt_close_bracket(s, l));

  // post message to clear console
  CConsoleMessage newMsg = {clearConsole, wxEmptyString};
  wxGetApp().console->mMsgQueue.Post(newMsg);

  // send event to queue
  wxQueueEvent(wxGetApp().console, new wxThreadEvent(wxEVT_THREAD, wxEVT_CONSOLE_MESSAGE));

  return result;
}


int __Clear(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_attempt_close_bracket(s, l));

  // post message with text to pe printed
  CDrawMessage newMsg = { clearScreen, 0, 0, 0, 0, wxNullColour };
  wxGetApp().screen->mPanelScreen->mMsgQueue.Post(newMsg);
  
  return result;
}


int __Draw(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  int x1, y1;
  int x2, y2;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_int(s,  l, &x1));
  mb_check(mb_pop_int(s,  l, &y1));
  
  mb_check(mb_pop_int(s,  l, &x2));
  mb_check(mb_pop_int(s,  l, &y2));
  mb_check(mb_attempt_close_bracket(s, l));

  CDrawMessage newMsg = { drawLine, x1, y1, x2, y2, wxNullColour };
  wxGetApp().screen->mPanelScreen->mMsgQueue.Post(newMsg);
  
  return result;
}


int __Color(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  int rgb;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_int(s,  l, &rgb));
  mb_check(mb_attempt_close_bracket(s, l));
  
  CDrawMessage newMsg = { setColour, 0, 0, 0, 0, wxColour(rgb) };
  wxGetApp().screen->mPanelScreen->mMsgQueue.Post(newMsg);
  
  return result;
}


int __RGB(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  int r, g, b;
  int rgb;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_int(s,  l, &r));
  mb_check(mb_pop_int(s,  l, &g));
  mb_check(mb_pop_int(s,  l, &b));
  mb_check(mb_attempt_close_bracket(s, l));
  
  rgb = 256*256*b + 256*g + r;
  mb_check(mb_push_int(s, l, rgb));
  
  return result;
}


int __OnStepped(struct mb_interpreter_t* s, void** l, const char* f, int p, unsigned short row, unsigned short col)
{
  mb_unrefvar(s);
  mb_unrefvar(l);
  mb_unrefvar(f);
  mb_unrefvar(p);
  mb_unrefvar(col);

  int result = MB_EXTENDED_ABORT;
  
  // test if this thread should end
  if(wxGetApp().frame->GetThread()->TestDestroy())
  {
    //char* frames[16];
    //int i = 0;
    
    //mb_check(mb_debug_get_stack_trace(s, l, frames, 16));

    //SendInfoText(wxString::Format("Frame: %s (%s %d %hu %hu)\n", frames[0], f, p, row, col));
        
    wxThreadEvent* event = new wxThreadEvent(wxEVT_THREAD, wxEVT_OUTPUT_GOTO_LINE);
    CCaretPosition caretPosition = { row, col };
    event->SetPayload(caretPosition);
    wxQueueEvent(wxGetApp().frame, event);

    result = mb_raise_error(s, l, SE_EA_EXTENDED_ABORT, MB_EXTENDED_ABORT + 100);

    return result;
  }
  
  //SendInfoText(wxString::Format("Here in row %hu, col %hu\n", row, col));
  
  return MB_FUNC_OK;
}


void __OnError(struct mb_interpreter_t* s, mb_error_e e, const char* m, const char* f, int p, unsigned short row, unsigned short col, int abort_code) {
  mb_unrefvar(s);
  mb_unrefvar(p);

  if(e != SE_NO_ERR) {
    if(f) {
      if(e == SE_RN_WRONG_FUNCTION_REACHED) {
        SendInfoText(wxString::Format(
          "Error:\n    Ln %d, Col %d in Func: %s, Code %d, Abort Code %d\n    Message: %s.\n",
          row, col, f,
          e, abort_code,
          m
        ));
      } else {
        SendInfoText(wxString::Format(
          "Error:\n    Ln %d, Col %d in File: %s, Code %d, Abort Code %d\n    Message: %s.\n",
          row, col, f,
          e, e == SE_EA_EXTENDED_ABORT ? abort_code - MB_EXTENDED_ABORT : abort_code,
          m
        ));
      }
    } else {
      SendInfoText(wxString::Format(
        "Error:\n    Ln %d, Col %d, Code %d, Abort Code %d\n    Message: %s.\n",
        row, col,
        e, e == SE_EA_EXTENDED_ABORT ? abort_code - MB_EXTENDED_ABORT : abort_code,
        m
      ));
    }
  }
  
  wxThreadEvent* event = new wxThreadEvent(wxEVT_THREAD, wxEVT_OUTPUT_GOTO_LINE);
  CCaretPosition caretPosition = { row, col };
  event->SetPayload(caretPosition);
  wxQueueEvent(wxGetApp().frame, event);
}


static int _file_open(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  char* fn = 0;
  char* fm = 0;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_string(s, l, &fn));
  mb_check(mb_pop_string(s, l, &fm));
  mb_check(mb_attempt_close_bracket(s, l));

  if(!fn) {
    result = MB_FUNC_ERR;

    goto _exit;
  }

  fp = fopen(fn, fm);
  if(!fp) {
    result = MB_FUNC_ERR;

    goto _exit;
  }

_exit:
  mb_check(mb_push_usertype(s, l, (void*)fp));

  return result;
}


static int _file_close(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  void* up = 0;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_usertype(s, l, &up));
  mb_check(mb_attempt_close_bracket(s, l));

  if(!up)
    return MB_FUNC_ERR;

  fp = (FILE*)up;
  fclose(fp);

  return result;
}

static int _file_peek(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  void* up = 0;
  long ft = -1; /* Push -1 for error */

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_usertype(s, l, &up));
  mb_check(mb_attempt_close_bracket(s, l));

  if(!up) {
    result = MB_FUNC_ERR;

    goto _exit;
  }

  fp = (FILE*)up;
  ft = ftell(fp);

_exit:
  mb_check(mb_push_int(s, l, ft));

  return result;
}


static int _file_poke(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  void* up = 0;
  int fo = 0;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_usertype(s, l, &up));
  mb_check(mb_pop_int(s, l, &fo));
  mb_check(mb_attempt_close_bracket(s, l));

  if(!up)
    return MB_FUNC_ERR;

  fp = (FILE*)up;
  fseek(fp, fo, SEEK_SET);

  return result;
}


static int _file_read(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  void* up = 0;
  int ln = 0;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_usertype(s, l, &up));
  if(mb_has_arg(s, l)) {
    mb_check(mb_pop_int(s, l, &ln));
  }
  mb_check(mb_attempt_close_bracket(s, l));

  if(!up) {
    result = MB_FUNC_ERR;

    goto _exit;
  }

  fp = (FILE*)up;
  if(ln) {
    char* buf = (char*)malloc(ln + 1);
    fgets(buf, ln + 1, fp);
    buf[ln] = '\0';
    mb_check(mb_push_string(s, l, mb_memdup(buf, ln + 1)));
    free(buf);
  } else {
    int ret = fgetc(fp);
    mb_check(mb_push_int(s, l, ret));
  }

_exit:
  return result;
}


static int _file_read_line(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  void* up = 0;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_usertype(s, l, &up));
  mb_check(mb_attempt_close_bracket(s, l));

  if(!up) {
    result = MB_FUNC_ERR;

    goto _exit;
  }

  fp = (FILE*)up;
  {
    char* buf = 0;
    size_t size = 0;
    while(!feof(fp)) {
      buf = (char*)realloc(buf, ++size);
      buf[size - 1] = (char)fgetc(fp);
      if(buf[size - 1] == '\n' || buf[size - 1] == '\r') {
        long l = ftell(fp);
        if(fgetc(fp) != '\r')
          fseek(fp, l, SEEK_SET);

        break;
      }
    }
    if(buf) {
      buf[size - 1] = '\0';
      mb_check(mb_push_string(s, l, mb_memdup(buf, size)));
    } else {
      mb_check(mb_push_string(s, l, mb_memdup("", 1)));
    }
    if(buf)
      free(buf);
  }

_exit:
  return result;
}


static int _file_write(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  void* up = 0;
  mb_value_t val;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));

  mb_check(mb_pop_usertype(s, l, &up));

  if(!up) {
    result = MB_FUNC_ERR;

    goto _exit;
  }

  fp = (FILE*)up;

  while(mb_has_arg(s, l)) { /* Support variadic */
    mb_check(mb_pop_value(s, l, &val));
    switch(val.type) {
    case MB_DT_INT:
      fputc(val.value.integer, fp);

      break;
    case MB_DT_REAL:
      fputc((int_t)val.value.float_point, fp);

      break;
    case MB_DT_STRING:
      fputs(val.value.string, fp);

      break;
    default:
      result = MB_FUNC_ERR;

      break;
    }
  }

_exit:
  mb_check(mb_attempt_close_bracket(s, l));

  return result;
}


static int _file_write_line(struct mb_interpreter_t* s, void** l)
{
  int result = MB_FUNC_OK;
  FILE* fp = 0;
  void* up = 0;
  mb_value_t val;

  mb_assert(s && l);

  mb_check(mb_attempt_open_bracket(s, l));
  mb_check(mb_pop_usertype(s, l, &up));

  if(!up) {
    result = MB_FUNC_ERR;

    goto _exit;
  }

  fp = (FILE*)up;

  while(mb_has_arg(s, l)) { /* Support variadic */
    mb_check(mb_pop_value(s, l, &val));
    switch(val.type) {
    case MB_DT_INT:
      fputc(val.value.integer, fp);

      break;
    case MB_DT_REAL:
      fputc((int_t)val.value.float_point, fp);

      break;
    case MB_DT_STRING:
      fputs(val.value.string, fp);

      break;
    default:
      result = MB_FUNC_ERR;

      break;
    }
  }

  fputc('\n', fp);
  fputc('\r', fp);

_exit:
  mb_check(mb_attempt_close_bracket(s, l));

  return result;
}


CMyBasic::CMyBasic() :
  mInterpreter(NULL)
{
  // initialise interpreter
  mb_init();
  mb_open(&mInterpreter);

  // set error handling and stepped handler functions
  mb_debug_set_stepped_handler(mInterpreter, __OnStepped);
  mb_set_error_handler(mInterpreter, __OnError);
  
  // replace printer and inputer
  mb_set_printer(mInterpreter, __Print);
  mb_set_inputer(mInterpreter, __Input);
  
  // Send version information to output
  SendInfoText(wxString::Format("MY-BASIC Interpreter Shell - %s\n", mb_ver_string()));
  SendInfoText("Copyright (C) 2011 - 2020 Wang Renxin. All Rights Reserved.\n");

  // register console function
  mb_register_func(mInterpreter, "CLS", __CLS);
  
  // register graphics function
  mb_begin_module(mInterpreter, "GFX");
  mb_register_func(mInterpreter, "Draw", __Draw);  // Draw [x1, y1] [To x2, y2][To x3, y3]...
  mb_register_func(mInterpreter, "Color", __Color);  // Color f%, b%
  mb_register_func(mInterpreter, "RGB", __RGB);  // Color f%, b%
  mb_register_func(mInterpreter, "Clear", __Clear);  // Cls [color]
  //mb_reg_fun(mInterpreter, Circle);  // Circle x, y, r [,w1, w2]
  mb_end_module(mInterpreter);
  
  // register file functions
  mb_register_func(mInterpreter, "FOPEN", _file_open);
  mb_register_func(mInterpreter, "FCLOSE", _file_close);
  mb_register_func(mInterpreter, "FPEEK", _file_peek);
  mb_register_func(mInterpreter, "FPOKE", _file_poke);
  mb_register_func(mInterpreter, "FREAD", _file_read);
  mb_register_func(mInterpreter, "FREAD_LINE", _file_read_line);
  mb_register_func(mInterpreter, "FWRITE", _file_write);
  mb_register_func(mInterpreter, "FWRITE_LINE", _file_write_line);
}


CMyBasic::~CMyBasic()
{
  mb_close(&mInterpreter);
  mb_dispose();
}


void CMyBasic::RunString(wxString codeString)
{
  int result;
  
  mb_load_string(mInterpreter, codeString.c_str(), true);
  mb_run(mInterpreter, true);
  
  result = mb_reset(&mInterpreter, false);

  mb_gc(mInterpreter, 0);
}
