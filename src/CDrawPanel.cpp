/////////////////////////////////////////////////////////////////////////////
// Name:        CDrawPanel.cpp
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Sunday, 07 June 2020 at 21:54:05
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
#include "wx/mstream.h"
////@end includes
#include <wx/dcbuffer.h>

// wxMyBASIC headers
#include "CMyBasic.h"
#include "app.h"
#include "CDrawPanel.h"

////@begin XPM images
////@end XPM images


/*
 * CDrawPanel type definition
 */

IMPLEMENT_DYNAMIC_CLASS( CDrawPanel, wxPanel )


/*
 * CDrawPanel event table definition
 */

BEGIN_EVENT_TABLE( CDrawPanel, wxPanel )
////@begin CDrawPanel event table entries
  EVT_SIZE( CDrawPanel::OnSize )
  EVT_PAINT( CDrawPanel::OnPaint )
////@end CDrawPanel event table entries
  EVT_TIMER(wxID_ANY, CDrawPanel::OnTimer)
END_EVENT_TABLE()


/*
 * CDrawPanel constructors
 */

CDrawPanel::CDrawPanel()
{
  Init();
}

CDrawPanel::CDrawPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
  Init();
  Create(parent, id, pos, size, style);
}


/*
 * CDrawPanel creator
 */

bool CDrawPanel::Create(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
////@begin CDrawPanel creation
  wxPanel::Create(parent, id, pos, size, style);
  CreateControls();
////@end CDrawPanel creation
    return true;
}


/*
 * CDrawPanel destructor
 */

CDrawPanel::~CDrawPanel()
{
////@begin CDrawPanel destruction
////@end CDrawPanel destruction
}


/*
 * Member initialisation
 */

void CDrawPanel::Init()
{
////@begin CDrawPanel member initialisation
////@end CDrawPanel member initialisation
  
  SetBackgroundStyle(wxBG_STYLE_PAINT);

  mTimer.SetOwner(this);
  mTimer.Start(50);
}


/*
 * Control creation for CDrawPanel
 */

void CDrawPanel::CreateControls()
{    
////@begin CDrawPanel content construction
////@end CDrawPanel content construction
}


/*
 * Should we show tooltips?
 */

bool CDrawPanel::ShowToolTips()
{
  return true;
}

/*
 * Get bitmap resources
 */

wxBitmap CDrawPanel::GetBitmapResource( const wxString& name )
{
  // Bitmap retrieval
////@begin CDrawPanel bitmap retrieval
  wxUnusedVar(name);
  return wxNullBitmap;
////@end CDrawPanel bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon CDrawPanel::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin CDrawPanel icon retrieval
  wxUnusedVar(name);
  return wxNullIcon;
////@end CDrawPanel icon retrieval
}

/*
 * wxEVT_SIZE event handler for ID_PANEL_SCREEN
 */

void CDrawPanel::OnSize( wxSizeEvent& event )
{
  // is ignored in the moment
  //wxSize size=event.GetSize();
  
  //ResizeBitmap(size.GetWidth(), size.GetHeight());
  event.Skip();
}


/*
 * wxEVT_PAINT event handler for ID_PANEL_SCREEN
 */

void CDrawPanel::OnPaint( wxPaintEvent& event )
{
  wxBufferedPaintDC dc(this);
  
  CDrawMessage msg;
  unsigned int nCommands=0;

  // process all messages in queue
  while ((mMsgQueue.ReceiveTimeout(0, msg)!= wxMSGQUEUE_TIMEOUT) && (nCommands++<1000))
  {
    switch (msg.type)
    {
    case clearScreen:
      dc.SetBrush(mCurrentColour);
      dc.SetBackground(dc.GetBrush());
      dc.Clear();
      break;
    case drawLine:
      dc.SetPen(mCurrentColour);
      if((msg.x1==msg.x2) && (msg.y1==msg.y2))
        dc.DrawPoint(msg.x1, msg.y1);
      else
        dc.DrawLine(msg.x1, msg.y1, msg.x2, msg.y2);
      break;
    case setColour:
      mCurrentColour = msg.colour;
      break;
    }
  }
}


void CDrawPanel::OnTimer(wxTimerEvent& event)
{
  Refresh();
}
