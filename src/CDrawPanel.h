/////////////////////////////////////////////////////////////////////////////
// Name:        CDrawPanel.h
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Sunday, 07 June 2020 at 21:54:05
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _CDRAWPANEL_H_
#define _CDRAWPANEL_H_


/*!
 * Includes
 */

////@begin includes
////@end includes
#include "wx/timer.h"
#include <wx/msgqueue.h>

/*!
 * Forward declarations
 */

////@begin forward declarations
class CDrawPanel;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_PANEL_SCREEN 10001
#define SYMBOL_CDRAWPANEL_STYLE wxNO_BORDER|wxTAB_TRAVERSAL
#define SYMBOL_CDRAWPANEL_IDNAME ID_PANEL_SCREEN
#define SYMBOL_CDRAWPANEL_SIZE wxSize(320, 200)
#define SYMBOL_CDRAWPANEL_POSITION wxDefaultPosition
////@end control identifiers

enum CDrawMsgType { clearScreen=0, drawLine, setColour };

struct CDrawMessage
{
  CDrawMsgType type;
  int x1, y1, x2, y2;
  wxColour colour;
};


/*!
 * CDrawPanel class declaration
 */

class CDrawPanel: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( CDrawPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    CDrawPanel();
    CDrawPanel(wxWindow* parent, wxWindowID id = ID_PANEL_SCREEN, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxSUNKEN_BORDER|wxTAB_TRAVERSAL);

    /// Creation
    bool Create(wxWindow* parent, wxWindowID id = ID_PANEL_SCREEN, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxSUNKEN_BORDER|wxTAB_TRAVERSAL);

    /// Destructor
    ~CDrawPanel();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin CDrawPanel event handler declarations

  /// wxEVT_SIZE event handler for ID_PANEL_SCREEN
  void OnSize( wxSizeEvent& event );

  /// wxEVT_PAINT event handler for ID_PANEL_SCREEN
  void OnPaint( wxPaintEvent& event );

////@end CDrawPanel event handler declarations

  void OnTimer(wxTimerEvent& event);
  
////@begin CDrawPanel member function declarations

  /// Retrieves bitmap resources
  wxBitmap GetBitmapResource( const wxString& name );

  /// Retrieves icon resources
  wxIcon GetIconResource( const wxString& name );
////@end CDrawPanel member function declarations

  /// Should we show tooltips?
  static bool ShowToolTips();
  
////@begin CDrawPanel member variables
////@end CDrawPanel member variables

  wxTimer mTimer;
  
  wxMessageQueue<CDrawMessage> mMsgQueue;

  wxColour mCurrentColour;
};

#endif
    // _CDRAWPANEL_H_
