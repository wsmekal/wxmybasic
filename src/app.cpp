/////////////////////////////////////////////////////////////////////////////
// Name:        src\app.cpp
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Friday, 05 June 2020 at 22:47:21
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

// wxWidgets headers
#include "wx/stdpaths.h"

////@begin includes
////@end includes

#include "version.h"
#include "app.h"
#include "frame.h"
#include "CConsoleFrame.h"
#include "CPaintFrame.h"
#include "CMyBasic.h"

////@begin XPM images
////@end XPM images


/*
 * Application instance implementation
 */

////@begin implement app
IMPLEMENT_APP( MyBASICApp )
////@end implement app


/*
 * MyBASICApp type definition
 */

IMPLEMENT_CLASS( MyBASICApp, wxApp )


/*
 * MyBASICApp event table definition
 */

BEGIN_EVENT_TABLE( MyBASICApp, wxApp )

////@begin MyBASICApp event table entries
////@end MyBASICApp event table entries

END_EVENT_TABLE()


/*
 * Constructor for MyBASICApp
 */

MyBASICApp::MyBASICApp()
{
    Init();
}


/*
 * Member initialisation
 */

void MyBASICApp::Init()
{
////@begin MyBASICApp member initialisation
////@end MyBASICApp member initialisation
  
  frame = NULL;
  console = NULL;
  screen = NULL;
}

/*
 * Initialisation for MyBASICApp
 */

bool MyBASICApp::OnInit()
{    
////@begin MyBASICApp initialisation
	// Remove the comment markers above and below this block
	// to make permanent changes to the code.

#if wxUSE_XPM
	wxImage::AddHandler(new wxXPMHandler);
#endif
#if wxUSE_LIBPNG
	wxImage::AddHandler(new wxPNGHandler);
#endif
#if wxUSE_LIBJPEG
	wxImage::AddHandler(new wxJPEGHandler);
#endif
#if wxUSE_GIF
	wxImage::AddHandler(new wxGIFHandler);
#endif
////@end MyBASICApp initialisation

  // create version string
  mVersionString = wxString::Format("%d.%04d", VERSION_MAJOR, REVISION);

  // determine data directories
  mUserDataDir = wxStandardPaths::Get().GetUserDataDir();
#ifdef __LINUX__
  // Linux: path to executable
  wxFileName exePath(wxStandardPaths::Get().GetExecutablePath());
  QLogic.m_DataDir = exePath.GetPath();
#else  // __LINUX__
  // Windows: path to executable (returned by GetResourcesDir())
  // Mac OS X: Resources path in app bundle
  mResourcesDir = wxStandardPaths::Get().GetResourcesDir().c_str();
#endif // __LINUX__

  // create user data directories if it doesn't exist
  if (!wxDirExists(mUserDataDir))
    if (!wxMkdir(mUserDataDir)) {
      wxLogError("Could not create user data directory (%s)!\n"
        "It is not possible to recover from this error!\n", mUserDataDir.c_str());
      return false;
    }

  wxString mLogFilename = mUserDataDir + "/log.txt";
  mLogFile = fopen(mLogFilename.c_str(), "w+");
  if (mLogFile != NULL)
    delete wxLog::SetActiveTarget(new wxLogStderr(mLogFile));
  //wxLogChain *logChain = new wxLogChain(new wxLogStderr(mLogFile));
  else {
    wxLogError("Could not open log file (%s)!\n"
      "It is not possible to recover from this error!\n", mLogFilename.c_str());
    return false;
  }

  // default log settings
  wxLog::SetLogLevel(wxLOG_Max);
  bool verboseLevel = true;
  wxLog::SetVerbose(verboseLevel);

  wxLogVerbose(wxString::Format("User Data Dir: %s", mUserDataDir));

  // create the main application window
  frame = new MainFrame(NULL);
  
  // create console frame
  console = new CConsoleFrame(frame);
  
  // create screen (to draw to)
  screen = new CPaintFrame(frame);
  screen->Fit();
  
  // and show it (the frames, unlike simple controls, are not shown when
  // created initially)
  frame->Show(true);
  
  // create basic interpreter (frame already exists - needed for version output)
  myBasic = new CMyBasic;

  return true;
}


/*
 * Cleanup for MyBASICApp
 */

int MyBASICApp::OnExit()
{    
////@begin MyBASICApp cleanup
	return wxApp::OnExit();
////@end MyBASICApp cleanup
}

