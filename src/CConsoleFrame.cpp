/////////////////////////////////////////////////////////////////////////////
// Name:        CConsoleFrame.cpp
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Saturday, 06 June 2020 at 20:13:06
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
#include "wx/mstream.h"
////@end includes

// wxWidgets headers
#include <wx/msgqueue.h>

// wxMyBASIC headers
#include "CConsoleFrame.h"

////@begin XPM images
////@end XPM images



/*
 * CConsoleFrame type definition
 */

IMPLEMENT_CLASS( CConsoleFrame, wxFrame )


/*
 * CConsoleFrame event table definition
 */

BEGIN_EVENT_TABLE( CConsoleFrame, wxFrame )

////@begin CConsoleFrame event table entries
  EVT_CLOSE( CConsoleFrame::OnCloseWindow )
////@end CConsoleFrame event table entries
  EVT_THREAD(wxEVT_OUTPUT_CONSOLE, CConsoleFrame::OnOutputConsole)
  EVT_THREAD(wxEVT_CONSOLE_CLEAR, CConsoleFrame::OnClearConsole)
  EVT_THREAD(wxEVT_CONSOLE_MESSAGE, CConsoleFrame::OnMessageConsole)
END_EVENT_TABLE()


/*
 * CConsoleFrame constructors
 */

CConsoleFrame::CConsoleFrame()
{
    Init();
}

CConsoleFrame::CConsoleFrame( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}


/*
 * CConsoleFrame creator
 */

bool CConsoleFrame::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin CConsoleFrame creation
  wxFrame::Create( parent, id, caption, pos, size, style );

  CreateControls();
  if (GetSizer())
  {
    GetSizer()->SetSizeHints(this);
  }
  Centre();
////@end CConsoleFrame creation

#if defined(__WXMSW__)
    mTextctrlShell->SetFont(wxFont(12, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Consolas")));
#elif defined(__WXOSX__)
    mTextctrlShell->SetFont(wxFont(13, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Monaco")));
#else
    mTextctrlShell->SetFont(wxFont(13, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Monaco")));
#endif

  // taken from https://stackoverflow.com/questions/49468153/is-there-a-direct-way-to-set-wx-textctrl-size-by-characters-rather-than-pixels
  int width, height;
  mTextctrlShell->GetTextExtent("X", &width, &height);
  wxSize shellSize(width * 80, height * 26);
  mTextctrlShell->SetInitialSize(shellSize);

  // taken from https://forums.wxwidgets.org/viewtopic.php?t=42346
  //this->Layout(); // force new calculation of sizer.
  mBoxSizerVConsole->Fit(this);

  //wxAcceleratorEntry entries[1];
  //entries[0].Set(wxACCEL_NORMAL, WXK_F5, ID_NEW_WINDOW);
  //wxAcceleratorTable accel(1, entries);
  //frame->SetAcceleratorTable(accel);

  return true;
}


/*
 * CConsoleFrame destructor
 */

CConsoleFrame::~CConsoleFrame()
{
////@begin CConsoleFrame destruction
////@end CConsoleFrame destruction
}


/*
 * Member initialisation
 */

void CConsoleFrame::Init()
{
////@begin CConsoleFrame member initialisation
  mBoxSizerVConsole = NULL;
  mTextctrlShell = NULL;
////@end CConsoleFrame member initialisation

  mEmptyQueue = true;
}


/*
 * Control creation for CConsoleFrame
 */

void CConsoleFrame::CreateControls()
{    
////@begin CConsoleFrame content construction
  CConsoleFrame* itemFrame1 = this;

  wxBoxSizer* itemBoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
  itemFrame1->SetSizer(itemBoxSizer1);

  wxPanel* itemPanel2 = new wxPanel( itemFrame1, ID_PANEL_CONSOLE, wxDefaultPosition, wxDefaultSize, wxNO_BORDER|wxTAB_TRAVERSAL );
  itemBoxSizer1->Add(itemPanel2, 0, wxALIGN_CENTER_VERTICAL, 5);

  mBoxSizerVConsole = new wxBoxSizer(wxVERTICAL);
  itemPanel2->SetSizer(mBoxSizerVConsole);

  mTextctrlShell = new wxTextCtrl( itemPanel2, ID_TEXTCTRL_SHELL, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
  mBoxSizerVConsole->Add(mTextctrlShell, 1, wxGROW, 5);

////@end CConsoleFrame content construction
}


/*
 * Should we show tooltips?
 */

bool CConsoleFrame::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap CConsoleFrame::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin CConsoleFrame bitmap retrieval
  wxUnusedVar(name);
  return wxNullBitmap;
////@end CConsoleFrame bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon CConsoleFrame::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin CConsoleFrame icon retrieval
  wxUnusedVar(name);
  return wxNullIcon;
////@end CConsoleFrame icon retrieval
}


/*
 * wxEVT_CLOSE_WINDOW event handler for ID_CCONSOLEFRAME
 */

void CConsoleFrame::OnCloseWindow( wxCloseEvent& event )
{
  if (event.CanVeto())
  {
    event.Veto();
    Show(false);
  }
  else
    event.Skip();
}


void CConsoleFrame::OnOutputConsole(wxThreadEvent& event)
{
  wxString output = event.GetString();

  mTextctrlShell->AppendText(output);
  
  Iconize(false); // restore the window if minimized
  SetFocus();  // focus on my window
  Raise();  // bring window to front
  Show(true); // show the window
}


void CConsoleFrame::OnClearConsole(wxThreadEvent& event)
{
  mTextctrlShell->Clear();
  
  Iconize(false); // restore the window if minimized
  SetFocus();  // focus on my window
  Raise();  // bring window to front
  Show(true); // show the window
}


void CConsoleFrame::OnMessageConsole(wxThreadEvent& event)
{
  CConsoleMessage msg;

  mEmptyQueueCS.Enter();
  mEmptyQueue = true;
  mEmptyQueueCS.Leave();

  // mTextctrlShell->AppendText("OnMessageConsole\n");

  wxString textToAppend = wxEmptyString;

  // process all messages in queue
  while (mMsgQueue.ReceiveTimeout(0, msg)!= wxMSGQUEUE_TIMEOUT)
  {
    switch (msg.type)
    {
    case clearConsole:
      mTextctrlShell->Clear();
      break;
    case outputText:
        textToAppend.Append(msg.text);
      break;
    }
  }
  
  if(!textToAppend.IsEmpty())
  {
    mTextctrlShell->Freeze();
    mTextctrlShell->AppendText(textToAppend);
    mTextctrlShell->Thaw();
  }

  Iconize(false); // restore the window if minimized
  SetFocus();  // focus on my window
  Raise();  // bring window to front
  Show(true); // show the window
}


