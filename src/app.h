/////////////////////////////////////////////////////////////////////////////
// Name:        src\mybasicapp.h
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Friday, 05 June 2020 at 22:47:21
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _APP_H_
#define _APP_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/image.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

class MainFrame;
class CMyBasic;
class CPaintFrame;
class CConsoleFrame;

/*!
 * Control identifiers
 */

////@begin control identifiers
////@end control identifiers

/*!
 * MyBASICApp class declaration
 */

class MyBASICApp: public wxApp
{    
    DECLARE_CLASS( MyBASICApp )
    DECLARE_EVENT_TABLE()

public:
    /// Constructor
    MyBASICApp();

    void Init();

    /// Initialises the application
    virtual bool OnInit();

    /// Called on exit
    virtual int OnExit();

////@begin MyBASICApp event handler declarations

////@end MyBASICApp event handler declarations

////@begin MyBASICApp member function declarations

////@end MyBASICApp member function declarations

////@begin MyBASICApp member variables
////@end MyBASICApp member variables
  
  // pointer to main frame
  MainFrame* frame;

  // pointer to main frame
  CConsoleFrame* console;
  
  // pointer to main frame
  CPaintFrame* screen;
  
  // Pointer to CMyBasic object;
  CMyBasic* myBasic;

  wxString mUserDataDir;   //!< full path to user data directory
  wxString mResourcesDir;   //!< full path to user data directory

private:
  FILE* mLogFile;   //!< file handle to log file, where all messages are stored (until restart)
  wxString mVersionString;   //!< Version of NoteSearch (Major version + Revision, e.g. "1.086")
};

/*!
 * Application instance declaration 
 */

////@begin declare app
DECLARE_APP(MyBASICApp)
////@end declare app

#endif
    // _APP_H_
