
#ifndef _CMYBASIC_H_
#define _CMYBASIC_H_

// wxWidgets headers
#include <wx/msgqueue.h>

// wxMyBASIC headers
#include "my_basic.h"


struct CCaretPosition
{
  int row;
  int col;
};


struct CMyBasicMessage
{
  wxString text;
};

class CMyBasic
{
public:
  CMyBasic(void);
  ~CMyBasic();

  void RunString(wxString codeString);

  wxMessageQueue<CMyBasicMessage>* GetMsgQueue(void)
  {
    return &mMsgQueue;
  }
  
private:
  // BASIC interpreter
  struct mb_interpreter_t* mInterpreter;

  // communication
  wxMessageQueue<CMyBasicMessage> mMsgQueue;
  
  // drawing
  wxColour currentColour;
};

#endif
// _CMYBASIC_H_
