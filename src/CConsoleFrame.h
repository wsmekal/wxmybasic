/////////////////////////////////////////////////////////////////////////////
// Name:        CConsoleFrame.h
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Saturday, 06 June 2020 at 20:17:21
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _CCONSOLEFRAME_H_
#define _CCONSOLEFRAME_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
////@end includes

// wxWidgets headers
#include <wx/msgqueue.h>

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxBoxSizer;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_CCONSOLEFRAME 10000
#define ID_PANEL_CONSOLE 10001
#define ID_TEXTCTRL_SHELL 10002
#define SYMBOL_CCONSOLEFRAME_STYLE wxCAPTION|wxSYSTEM_MENU|wxCLOSE_BOX
#define SYMBOL_CCONSOLEFRAME_TITLE _("Console")
#define SYMBOL_CCONSOLEFRAME_IDNAME ID_CCONSOLEFRAME
#define SYMBOL_CCONSOLEFRAME_SIZE wxDefaultSize
#define SYMBOL_CCONSOLEFRAME_POSITION wxDefaultPosition
////@end control identifiers

#define wxEVT_OUTPUT_CONSOLE        20000
#define wxEVT_CONSOLE_CLEAR         20001
#define wxEVT_CONSOLE_MESSAGE       20002

enum CConsoleMsgType { clearConsole, outputText };

struct CConsoleMessage
{
  CConsoleMsgType type;
  wxString text;
};



/*!
 * CConsoleFrame class declaration
 */

class CConsoleFrame: public wxFrame
{    
    DECLARE_CLASS( CConsoleFrame )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    CConsoleFrame();
    CConsoleFrame( wxWindow* parent, wxWindowID id = SYMBOL_CCONSOLEFRAME_IDNAME, const wxString& caption = SYMBOL_CCONSOLEFRAME_TITLE, const wxPoint& pos = SYMBOL_CCONSOLEFRAME_POSITION, const wxSize& size = SYMBOL_CCONSOLEFRAME_SIZE, long style = SYMBOL_CCONSOLEFRAME_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CCONSOLEFRAME_IDNAME, const wxString& caption = SYMBOL_CCONSOLEFRAME_TITLE, const wxPoint& pos = SYMBOL_CCONSOLEFRAME_POSITION, const wxSize& size = SYMBOL_CCONSOLEFRAME_SIZE, long style = SYMBOL_CCONSOLEFRAME_STYLE );

    /// Destructor
    ~CConsoleFrame();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin CConsoleFrame event handler declarations

  /// wxEVT_CLOSE_WINDOW event handler for ID_CCONSOLEFRAME
  void OnCloseWindow( wxCloseEvent& event );

////@end CConsoleFrame event handler declarations

////@begin CConsoleFrame member function declarations

  /// Retrieves bitmap resources
  wxBitmap GetBitmapResource( const wxString& name );

  /// Retrieves icon resources
  wxIcon GetIconResource( const wxString& name );
////@end CConsoleFrame member function declarations
  
  /// wxEVT_OUTPUT_TEXT event handler
  void OnOutputConsole(wxThreadEvent& event);

  /// wxEVT_CONSOLE_CLEAR event handler
  void OnClearConsole(wxThreadEvent& event);

  /// wxEVT_CONSOLE_MESSAGE event handler
  void OnMessageConsole(wxThreadEvent& event);

  /// Should we show tooltips?
  static bool ShowToolTips();

////@begin CConsoleFrame member variables
  wxBoxSizer* mBoxSizerVConsole;
  wxTextCtrl* mTextctrlShell;
////@end CConsoleFrame member variables

  wxMessageQueue<CConsoleMessage> mMsgQueue;

  bool mEmptyQueue;
  wxCriticalSection mEmptyQueueCS;
};

#endif
    // _CCONSOLEFRAME_H_
