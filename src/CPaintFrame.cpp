/////////////////////////////////////////////////////////////////////////////
// Name:        CPaintFrame.cpp
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Saturday, 06 June 2020 at 20:17:48
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
#include "wx/mstream.h"
#include "CDrawPanel.h"
////@end includes

#include "CPaintFrame.h"
#include "CMyBasic.h"

////@begin XPM images
////@end XPM images


/*
 * CPaintFrame type definition
 */

IMPLEMENT_CLASS( CPaintFrame, wxFrame )


/*
 * CPaintFrame event table definition
 */

BEGIN_EVENT_TABLE( CPaintFrame, wxFrame )
////@begin CPaintFrame event table entries
  EVT_CLOSE( CPaintFrame::OnCloseWindow )
////@end CPaintFrame event table entries
END_EVENT_TABLE()


/*
 * CPaintFrame constructors
 */

CPaintFrame::CPaintFrame()
{
    Init();
}

CPaintFrame::CPaintFrame( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}


/*
 * CPaintFrame creator
 */

bool CPaintFrame::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin CPaintFrame creation
  wxFrame::Create( parent, id, caption, pos, size, style );

  CreateControls();
  if (GetSizer())
  {
    GetSizer()->SetSizeHints(this);
  }
  Centre();
////@end CPaintFrame creation
    return true;
}


/*
 * CPaintFrame destructor
 */

CPaintFrame::~CPaintFrame()
{
////@begin CPaintFrame destruction
////@end CPaintFrame destruction
}


/*
 * Member initialisation
 */

void CPaintFrame::Init()
{
////@begin CPaintFrame member initialisation
  mPanelScreen = NULL;
////@end CPaintFrame member initialisation
}


/*
 * Control creation for CPaintFrame
 */

void CPaintFrame::CreateControls()
{    
////@begin CPaintFrame content construction
  CPaintFrame* itemFrame1 = this;

  wxBoxSizer* itemBoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
  itemFrame1->SetSizer(itemBoxSizer1);

  wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
  itemBoxSizer1->Add(itemBoxSizer2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 2);

  mPanelScreen = new CDrawPanel( itemFrame1, ID_PANEL_SCREEN, wxDefaultPosition, wxSize(320, 200), wxNO_BORDER|wxTAB_TRAVERSAL );
  itemBoxSizer2->Add(mPanelScreen, 0, wxALIGN_CENTER_VERTICAL, 0);

////@end CPaintFrame content construction
}


/*
 * Should we show tooltips?
 */

bool CPaintFrame::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap CPaintFrame::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin CPaintFrame bitmap retrieval
  wxUnusedVar(name);
  return wxNullBitmap;
////@end CPaintFrame bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon CPaintFrame::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin CPaintFrame icon retrieval
  wxUnusedVar(name);
  return wxNullIcon;
////@end CPaintFrame icon retrieval
}


/*
 * wxEVT_CLOSE_WINDOW event handler for ID_CPAINTFRAME
 */

void CPaintFrame::OnCloseWindow( wxCloseEvent& event )
{
  if (event.CanVeto())
  {
    event.Veto();
    Show(false);
  }
  else
    event.Skip();
}
