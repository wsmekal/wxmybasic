/////////////////////////////////////////////////////////////////////////////
// Name:        CPaintFrame.h
// Purpose:     
// Author:      Werner Smekal
// Modified by: 
// Created:     Saturday, 06 June 2020 at 20:17:48
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _CPAINTFRAME_H_
#define _CPAINTFRAME_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
class CDrawPanel;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_CPAINTFRAME 10038
#define SYMBOL_CPAINTFRAME_STYLE wxCAPTION|wxSYSTEM_MENU|wxCLOSE_BOX
#define SYMBOL_CPAINTFRAME_TITLE _("Screen")
#define SYMBOL_CPAINTFRAME_IDNAME ID_CPAINTFRAME
#define SYMBOL_CPAINTFRAME_SIZE wxDefaultSize
#define SYMBOL_CPAINTFRAME_POSITION wxDefaultPosition
////@end control identifiers

/*!
 * CPaintFrame class declaration
 */

class CPaintFrame: public wxFrame
{    
    DECLARE_CLASS( CPaintFrame )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    CPaintFrame();
    CPaintFrame( wxWindow* parent, wxWindowID id = SYMBOL_CPAINTFRAME_IDNAME, const wxString& caption = SYMBOL_CPAINTFRAME_TITLE, const wxPoint& pos = SYMBOL_CPAINTFRAME_POSITION, const wxSize& size = SYMBOL_CPAINTFRAME_SIZE, long style = SYMBOL_CPAINTFRAME_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CPAINTFRAME_IDNAME, const wxString& caption = SYMBOL_CPAINTFRAME_TITLE, const wxPoint& pos = SYMBOL_CPAINTFRAME_POSITION, const wxSize& size = SYMBOL_CPAINTFRAME_SIZE, long style = SYMBOL_CPAINTFRAME_STYLE );

    /// Destructor
    ~CPaintFrame();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin CPaintFrame event handler declarations

  /// wxEVT_CLOSE_WINDOW event handler for ID_CPAINTFRAME
  void OnCloseWindow( wxCloseEvent& event );

////@end CPaintFrame event handler declarations

////@begin CPaintFrame member function declarations

  /// Retrieves bitmap resources
  wxBitmap GetBitmapResource( const wxString& name );

  /// Retrieves icon resources
  wxIcon GetIconResource( const wxString& name );
////@end CPaintFrame member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin CPaintFrame member variables
  CDrawPanel* mPanelScreen;
////@end CPaintFrame member variables
};

#endif
    // _CPAINTFRAME_H_
