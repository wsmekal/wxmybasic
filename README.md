# wxMyBASIC

MyBASIC enhanced with simple IDE and GUI/draw functions.

# Prerequisites

## Mac OS X (tested on Catalina)

### Install wxWidgets library

* Download and unzip wxWidgets 3.1.3 "Source for Linux, macOS, etc" to $HOME/Development/wxWidgets-3.1.3 from here: http://wxwidgets.org/downloads/  
* Then compile the library from source (debug):
  * cd wxWidgets-3.1.3
  * mkdir build-cocoa-debug
  * cd build-cocoa-debug
  * ../configure --enable-debug --enable-shared --with-libjpeg=builtin --with-libtiff=builtin --with-zlib=builtin --with-libpng=builtin --with-expat=builtin --with-regex=builtin 
  * make -j 4
* Then compile the library from source (release):
  * cd wxWidgets-3.1.3
  * mkdir build-cocoa-release
  * cd build-cocoa-reelase
  * ../configure --disable-debug --enable-shared --with-libjpeg=builtin --with-libtiff=builtin --with-zlib=builtin --with-libpng=builtin --with-expat=builtin --with-regex=builtin 
  * make -j 4
* Test if all is okay. In build (debug and/or release) directory run:
  * cd samples; make; cd ..
  * Run some samples in directory

## Windows 10

### Install Visual C++ 2019 Community Edition

* Download here: [Visual Studio 2019 für Windows und Mac herunterladen](https://visualstudio.microsoft.com/de/downloads/)
* On page "Workloads" select "Desktopentwicklung mit C++""
* Select Details for installation on the right:
  * MSVC v142 - VS 2019 ...
  * Windows 10 SDK (latest version)
  * Just-in_time-Debugger
  * C++-Profilererstellungstool
  * C++-CMake-Tools für Windows
  * C++-ATL für die neuesten v142-Buildtools
  * C++ AddressSanitizer
  * Download and Install
  * Likely Restart

### Setup command line environment

* Create directory C:\Development
* I like to use cmder - so you like it too!
* Downloader cmder Mini from here https://cmder.net/
* Unzip package to C:\Development\cmder
* Create a shortlink to cmder.exe on the Desktop - right click on properties of shortcut and change "Ausführen in" to C:\Development
* Create with vim a file called profile.bat. Enter:
  * @echo off
  * REM Setup wxWidgets
  * set WXWIN=C:\Development\wxWidgets-3.1.3
* Before running cmake in the lea-ur10-scs directory, run the profile.bat batch file

### Install wxWidgets library

* Download and unzip wxWidgets 3.1.3 source code to C:\Development\wxWidgets-3.1.3 from here: http://wxwidgets.org/downloads/  
* Open wx_vc16.sln in build/msw in Visual Studio 2019 (open it twice, first time error occurs)
* Set "Debug" and "x64" and build with "Projektmappe erstellen (F7)"
* Set "Release" and "x64" and build with "Projektmappe erstellen (F7)""
* Test if all is okay
  * Open "minimal_vc15.sln" in samples/minimal
  * Set "Debug" and "x64" and build with "Projektmappe erstellen (F7)"
  * Run debug with F5

### Install cmake

* Download cmake win64-x64 Installer (3.17.3 at the time of writing) frome [Download | CMake](https://cmake.org/download/)
* Install - choose option "Add CMake to the system PATH for all users"

### Install Dialogblocks

* Download Version 5.16 standalone distribution from here http://dialogblocks.com/download.htm
* Install it
* Ask Werner Smekal for the Licence Code

# Obtain source code

* Start terminal
* Go to C:\Development
* Follow these instructions http://gitlab.xarion.com/help/ssh/README#generating-a-new-ssh-key-pair to create a ssh key and store it in the settings.
* Clone repository: git clone ssh://git@gitlab.xarion.com:222/application/lea-ur10-scs.git

# Compilation

## Mac OS X

* Open terminal
* cd Development/wxMyBASIC
* mkdir xcode
* cd xcode
* cmake -G "Xcode" -DCMAKE_PREFIX_PATH=$HOME/Development/wxWidgets-3.1.3/build-cocoa-debug ..
* open wxMyBASIC.xcodeproj

## Windows 10

* start cmd.exe or Cmder terminal
* setup environment
  * cd \Development
  * profile.bat
* Enter project directory and create build directory
  * cd lea_ur10_scs
  * mkdir visualc
  * cd visualc
* create Visual C++ Project File
  * cmake -G "Visual Studio 16 2019" -A x64 ..
  * open lea_ur10_scs.sln
* build "lea_ur10_scs"

# Miscellaneous

## Software

* N/A
